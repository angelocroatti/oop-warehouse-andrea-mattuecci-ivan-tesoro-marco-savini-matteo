package application.C;

import java.io.*;
import java.util.*;

public interface Consumi {
    
    /**
     * @param pasto
     * @param cliente
     * @param dates
     * @return i consumi nelle date selezionate
     */
    public NavigableMap<Date,HashMap<String,Float>> getConsumi(final Pasto pasto, final Cliente cliente, final Date ...dates) throws DateNotFound;
    
    /**
     * @param pasto
     * @param cliente
     * @param dates
     * @return la media dei consumi nelle date selezionate
     */
    public NavigableMap<Date,HashMap<String,Float>> getConsumoProCapite(final Pasto pasto, final Cliente cliente, final Date ...dates) throws DateNotFound;
    
    /**
     * @param pasto
     * @param cliente
     * @param dates
     * @return i consumi medi nelle date selezionate
     */
    public HashMap<String,Float> getConsumoMedioRangeDate(final Pasto pasto, final Cliente cliente, final Date ...dates) throws DateNotFound;
}
