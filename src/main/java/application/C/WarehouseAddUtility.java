package application.C;

import application.M.*;

/**
 * @author marco
 *
 */
public interface WarehouseAddUtility {

	/**
	 * Instantiate a new Tipologia
	 * 
	 * @param catena the container of typology
	 * @param id     the typology to be added
	 * @param key    the info to be added
	 * @param value  of the info to be added
	 * @return new Tipologia
	 */
	Tipologia addNewTipologia(final Catena catena, final String id, final String key, final String value);

	/**
	 * Instantiate a new Prodotto
	 * 
	 * @param catena       the container of the product
	 * @param idProdotto   the name of the product to be added
	 * @param idTipo       the typology of the product
	 * @param idScarto     the waste of the product
	 * @param valoreScarto the value of waste
	 * @param key          the name of the info
	 * @param value        the name of the info
	 * @return new Prodotto
	 */
	Prodotto addNewProdotto(final Catena catena, final String idProdotto, final String idTipo, final String idScarto,
			final String valoreScarto, final String key, final String value);

	/**
	 * Instantiate a new ProdConcreto
	 * 
	 * @param catena       the container of the product
	 * @param idProdCon    the name of the product to be added
	 * @param idProdotto   the "father" of the product
	 * @param idScarto     the waste of the product
	 * @param valoreScarto the value of waste
	 * @param key          the name of the info
	 * @param value        the value of the info
	 * @return new ProdConcreto
	 */
	ProdConcreto addNewProdConcreto(final Catena catena, final String idProdCon, final String idProdotto,
			final String idScarto, final String valoreScarto, final String key, final String value);

	/**
	 * Instantiate a new ProdFornito
	 * 
	 * @param catena         the container of the product
	 * @param idProdFor      the name of the product to be added
	 * @param idProdCon      the "father" of the product
	 * @param idScarto       the waste of the product
	 * @param valoreScarto   the value of waste
	 * @param key            the name of the info
	 * @param value          the value of the info
	 * @param idForn         the name of the supplier
	 * @param prezzo         the price of the product
	 * @param valoreAssoluto the absolute value of the product
	 * @return new ProdFornito
	 */
	ProdFornito addNewProdFornito(final Catena catena, final String idProdFor, final String idProdCon,
			final String idScarto, final String valoreScarto, final String key, final String value, final String idForn,
			final String prezzo, final String valoreAssoluto);


	/**
	 * Instantiate a new Hotel
	 * 
	 * @param catena  the container of the product
	 * @param idHotel the name of the hotel to be added
	 * @param info    the name of the info
	 * @return new Hotel
	 */
	Hotel addNewHotel(final Catena catena, final String idHotel, final String info);

	/**
	 * Instatiate a new Fornitore
	 * 
	 * @param catena
	 * @param idFornitore
	 * @return new Fornitore
	 */
	Fornitore addNewFornitore(final Catena catena, final String idFornitore);

	/**
	 * Instantiate a new Dispensa
	 * 
	 * @param catena     the container of the product
	 * @param idHotel    the name of the hotel
	 * @param idDispensa the name of the dispensa to be added
	 * @return new Dispensa
	 */
	Dispensa addNewDispensa(final Catena catena, final String idHotel, final String idDispensa);

}
