package application.V;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.sun.javafx.collections.MappingChange.Map;

import application.M.*;
import application.C.*;

public class GUI_OrganizzazioneUtentiLogicsImpl implements GUI_OrganizzazioneUtentiLogics{
	
	private Utente user;	
	private ArrayList<JCheckBox> checkBoxOnPanel;
	private HashMap<String, Utente> mappa;
	
	public GUI_OrganizzazioneUtentiLogicsImpl(){
		GUI.control=false; 
		checkBoxOnPanel = new ArrayList<JCheckBox>();	
		mappa = new HashMap<>();
	}
	
	
	
	/**
     * @return users map
     */
    public HashMap<String, Utente> getMappa() {
		return this.mappa;
	}

	/**
	 * @param mappa
	 * 
	 * set user map
	 */
	public void setMappa(HashMap<String, Utente> mappa) {
		this.mappa = mappa;
	}

	/**
	 * Read users saved
	 */
	public void leggiElencoConMAPPA() {

    	try {
			
			ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(new FileInputStream("Elenco.bin")));
			this.mappa = (HashMap) is.readObject();
			
			//System.out.println("chiave     valore");
			
			/*
			for(var x: mappa.keySet()) {
				System.out.println("Log:  "+x+"  "+mappa.get(x).getUsername()+"  "+mappa.get(x).getGrado());
			} 
			*/

			is.close();
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
    	
    }
    
	/**
     * @param tx
     * @param p
     * 
     * Save a new user
     */
    public void scriviNuovoUtenteConMAPPA(String tx, char[] p) {
	   
	   	this.user = new Utente(tx, p);	
	   	//user.setGrado(1); //solo per creare l'admin
	   	this.mappa.put(user.getUsername(), user);

		try {
			
			ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("Elenco.bin")));
			os.writeObject(this.mappa);
			os.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		System.out.println("Utente aggiunto, scrittura avvenuta");
	   
   }
    
    /**
     * Refresh users list
     */
    public void writeSulFileConMAPPA() {
    	
    	try {
			
			ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("Elenco.bin")));
			os.writeObject(this.mappa);
			os.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		System.out.println("Scrittura utenti aggiornata");
    	
    }
    
    /**
     * @param panel
     * 
     * Writes users on the panel
     */
    public void writePannelloConMAPPA(JPanel panel) {
    	
    	JCheckBox cb;
		panel.removeAll();
		panel.repaint();
		
    	this.leggiElencoConMAPPA();
    	
		for(var x : this.mappa.keySet()) {
			
			cb = new JCheckBox(mappa.get(x).getUsername());
			this.checkBoxOnPanel.add(cb);
			panel.add(cb);		
			panel.revalidate();
		}
    	
    }
    
    /**
     * Remove user selected
     */
    public void removeSelectedCheckboxConMAPPA() {
    	  	
    	for(var y : this.checkBoxOnPanel) {
    		
    		if(y.isSelected()) {
	    		
    			if(this.mappa.get(y.getText()).getGrado()!=1) {
	    			this.mappa.remove(y.getText());  		
    			}
	    		
    		}
    	}
    	
    	this.writeSulFileConMAPPA();
    }
    
    /**
     * Upgrade users 
     */
    public void promozioneConMAPPA() {
    	
    	this.leggiElencoConMAPPA();
    	
    	for(var y : this.checkBoxOnPanel) {
    		
    		if(y.isSelected()) {
	    		
    			if(this.mappa.get(y.getText()).getGrado()!=1) {
	    			this.mappa.get(y.getText()).setGrado(2);
	    			y.setSelected(false);
    			}
    		}
    	}
    	
    	this.writeSulFileConMAPPA();
    	
    	System.out.println("Utenti selezionati promossi");
    	
    }
    
    /**
     * Downgrade users 
     */
    public void declassamentoConMAPPA() {
    	
    	this.leggiElencoConMAPPA();
    	
    	for(var y : this.checkBoxOnPanel) {
    		
    		if(y.isSelected()) {
	    		
    			if(this.mappa.get(y.getText()).getGrado()!=1) {
	    			this.mappa.get(y.getText()).setGrado(3);
	    			y.setSelected(false);
    			}
    		}
    	}
    	
    	this.writeSulFileConMAPPA();
    	
    	System.out.println("Utenti selezionati declassati");
    }
    
    /**
     * @return true if only one user is selected
     */
    public boolean soloUnoSelezionato() {
    	
    	int cont=0;
    	
    	for(var x : this.checkBoxOnPanel) {
    		
    		if(x.isSelected()) {
    			cont++;
    		}
    		
    	}	
    	
    	if(cont==1) {
    		return true;
    	}
    	
    	cont = 0;
    	return false;
    }
    
    /**
     * @return the username of the selected user
     */
    public String getFirstSelected() {
    	for(var x : this.checkBoxOnPanel) {
    		
    		if(x.isSelected()) {
    			return this.mappa.get(x.getText()).getUsername();
    		}
    		
    	}
    	
    	return null;
    }
    
    /**
     * @param tx
     * @return true if Username is already presente, false in other cases
     */
    public boolean UtenteGiaPresente(String tx) {
    	
    	ObjectInputStream is;
	
		try {
			is = new ObjectInputStream(new BufferedInputStream(new FileInputStream("Elenco.bin")));
			this.mappa = (HashMap) is.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}

		System.out.println("chiave     valore");
		
		for(var x: mappa.keySet()) {
			if(mappa.get(x).getUsername().equals(tx)) {
				return true;
			}
		} 
    	
    	return false;
    
	}

    
	/**
	 * Return true if admin if present
	 */
	@Override
	public boolean AdminGiaPresente() {
	
		ObjectInputStream is;
		
		try {
			is = new ObjectInputStream(new BufferedInputStream(new FileInputStream("Elenco.bin")));
			this.mappa = (HashMap) is.readObject();
		} catch (IOException | ClassNotFoundException e) {
			
			//e.printStackTrace();
		}

		for(var x: mappa.keySet()) {
			if(mappa.get(x).getGrado() == 1) {
				return true;
			}
			System.out.println("Grado presente: "+mappa.get(x).getGrado());
		} 
    	
    	return false;
	}


	/**
	 * Create new admin
	 */
	public void CreaAdmin(String tx, char[] p) {
		
		this.user = new Utente(tx, p);	
	   	user.setGrado(1); //solo per creare l'admin
	   	this.mappa.put(user.getUsername(), user);

		try {
			
			ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("Elenco.bin")));
			os.writeObject(this.mappa);
			os.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		System.out.println("ADMIN aggiunto, scrittura avvenuta");
		
	}
}
