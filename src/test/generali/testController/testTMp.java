package generali.testController;

import static org.junit.jupiter.api.Assertions.*;
 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;
 
import org.junit.jupiter.api.Test;
 
import application.C.Consumi;
import application.C.Consumi.Cliente;
import application.C.Consumi.Pasto;
import application.C.ConsumiImpl;
import application.C.DateNotFound;
import application.C.ForecastNotAvailable;
import application.C.Previsioni;
import application.M.Catena;
import application.M.Dispensa;
import application.M.Hotel;
import application.M.ProdConcreto;
import application.M.ProdFornito;
import application.M.Prodotto;
import application.M.Tipologia;
import application.M.Typology;
 
class testTMp {
	
	    Previsioni previsioni;
	   
    Catena c;
    Hotel h;
    Date d;
    Date d2;
    Consumi consumi;
    ProdFornito tonno200gGeloService;
    ProdFornito vitello100gMacelleria;
   
    testTMp() {
        d = parseDate("01/08/2020");
        d2 = parseDate("02/08/2020");
       
        c = new Catena();
        h = new Hotel("Aragosta");
       
        c.getAlberghi().add(h);
       
        h.aggiungiUnNCena(d, 2, 2);
        h.aggiungiUnNPranzo(d2, 2, 2);
       
        h.aggiungiUnaDispensa(new Dispensa("Pesceria", c));
        Tipologia pesce = new Tipologia("Pesce");
        tonno200gGeloService =
                new ProdFornito("Tonno200gGeloService",
                        new ProdConcreto("Tonno200g",
                                new Prodotto("Tonno", pesce)));
        tonno200gGeloService.setValoreAssoluto((float) 0.1);
       
        h.aggiungiUnaDispensa(new Dispensa("Carne", c));
        Tipologia carne = new Tipologia("Carne");
        vitello100gMacelleria =
                new ProdFornito("Vitello100gMacelleria",
                        new ProdConcreto("Vitello100g",
                                new Prodotto("Vitello", carne)));
        vitello100gMacelleria.setValoreAssoluto((float) 0.1);
       
        ArrayList<Typology> t = new ArrayList<Typology>();
        t.add(pesce);
        t.add(carne);
        t.add(tonno200gGeloService);
        t.add(vitello100gMacelleria);
       
        c.aggiungiAllInventario(t);
       
        TreeMap<Date,HashMap<String,Float>> consumiGiornAdultiPranzo = new TreeMap<Date,HashMap<String,Float>>();
        TreeMap<Date,HashMap<String,Float>> consumiGiornBambiniCena = new TreeMap<Date,HashMap<String,Float>>();
       
        HashMap<String,Float> cons = new HashMap<String, Float>();
        cons.put(tonno200gGeloService.getID(), (float) 10);
       
        HashMap<String,Float> cons2 = new HashMap<String, Float>();
        cons2.put(vitello100gMacelleria.getID(), (float) 10);
       
        consumiGiornBambiniCena.put(d, cons);
       
        consumiGiornAdultiPranzo.put(d2, cons2);
       
        h.getConsumiAdultiPranzo().putAll(consumiGiornAdultiPranzo);
        h.getConsumiBimbiCena().putAll(consumiGiornBambiniCena);
       
        consumi = new ConsumiImpl(h);
    }
   
 
    @Test
    void testConsumi() {
        try {
			assertEquals(h.getConsumiAdultiPranzo(),consumi.getConsumi(Pasto.PRANZO, Cliente.ADULTO, d2));
		} catch (DateNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			assertEquals(h.getConsumiBimbiCena(), consumi.getConsumi(Pasto.CENA, Cliente.BAMBINO, d));
		} catch (DateNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    @Test
    void testConsumiProCapite() {
        TreeMap<Date,HashMap<String,Float>> consumiGiornBambiniCenaTest = new TreeMap<Date,HashMap<String,Float>>();
        HashMap<String,Float> consTest = new HashMap<String, Float>();
        consTest.put(tonno200gGeloService.getID(), (float) (10 / 2));
        consumiGiornBambiniCenaTest.put(d, consTest);
        try {
			assertEquals(consumiGiornBambiniCenaTest, consumi.getConsumoProCapite(Pasto.CENA, Cliente.BAMBINO, d));
		} catch (DateNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
        TreeMap<Date,HashMap<String,Float>> consumiGiornAdultiPranzoTest = new TreeMap<Date,HashMap<String,Float>>();
        HashMap<String,Float> cons2Test = new HashMap<String, Float>();
        cons2Test.put(vitello100gMacelleria.getID(), (float) (10 / 2));
        consumiGiornAdultiPranzoTest.put(d2, cons2Test);
        try {
			assertEquals(consumiGiornAdultiPranzoTest, consumi.getConsumoProCapite(Pasto.PRANZO, Cliente.ADULTO, d2));
		} catch (DateNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
    }
    @Test
    public void testPrevisioni() {
       
      try {
        previsioni.getPrevisioni(Pasto.PRANZO, Cliente.ADULTO, 1, d2);
        fail("Expected an ForecastNotAvailable to be thrown");
      } catch (ForecastNotAvailable f) {
        assertEquals(f.getMessage(), "Impossibile effettuare previsioni");
      }
    }
    
 
       
    public static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
 
}